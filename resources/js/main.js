// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {
  
  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 
      
    },
    finalize: function() {
      
    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 
      
    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here
 
    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;
 
    // hit up common first.
    UTIL.fire( 'common' );
 
    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);


/* QUOTE ROTATOR */

/**
 * jquery.cbpQTRotator.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
;( function( $, window, undefined ) {

  'use strict';

  // global
  var Modernizr = window.Modernizr;

  $.CBPQTRotator = function( options, element ) {
    this.$el = $( element );
    this._init( options );
  };

  // the options
  $.CBPQTRotator.defaults = {
    // default transition speed (ms)
    speed : 700,
    // default transition easing
    easing : 'ease',
    // rotator interval (ms)
    interval : 5000
  };

  $.CBPQTRotator.prototype = {
    _init : function( options ) {

      // options
      this.options = $.extend( true, {}, $.CBPQTRotator.defaults, options );
      // cache some elements and initialize some variables
      this._config();
      // show current item
      this.$items.eq( this.current ).addClass( 'cbp-qtcurrent' );
      // set the transition to the items
      if( this.support ) {
        this._setTransition();
      }
      // start rotating the items
      this._startRotator();

    },
    _config : function() {

      // the content items
      this.$items = this.$el.children( 'div.cbp-qtcontent' );
      // total items
      this.itemsCount = this.$items.length;
      // current item's index
      this.current = 0;
      // support for CSS Transitions
      this.support = Modernizr.csstransitions;
      // add the progress bar
      if( this.support ) {
        this.$progress = $( '<span class="cbp-qtprogress"></span>' ).appendTo( this.$el );
      }

    },
    _setTransition : function() {
      setTimeout( $.proxy( function() {
        this.$items.css( 'transition', 'opacity ' + this.options.speed + 'ms ' + this.options.easing );
      }, this ), 25 );
    },
    _startRotator: function() {

      if( this.support ) {
        this._startProgress();
      }

      setTimeout( $.proxy( function() {
        if( this.support ) {
          this._resetProgress();
        }
        this._next();
        this._startRotator();
      }, this ), this.options.interval );

    },
    _next : function() {

      // hide previous item
      this.$items.eq( this.current ).removeClass( 'cbp-qtcurrent' );
      // update current value
      this.current = this.current < this.itemsCount - 1 ? this.current + 1 : 0;
      // show next item
      this.$items.eq( this.current ).addClass('cbp-qtcurrent');

    },
    _startProgress : function() {
      
      setTimeout( $.proxy( function() {
        this.$progress.css( { transition : 'width ' + this.options.interval + 'ms linear', width : '100%' } );
      }, this ), 25 );

    },
    _resetProgress : function() {
      this.$progress.css( { transition : 'none', width : '0%' } );
    },
    destroy : function() {
      if( this.support ) {
        this.$items.css( 'transition', 'none' );
        this.$progress.remove();
      }
      this.$items.removeClass( 'cbp-qtcurrent' ).css( {
        'position' : 'relative',
        'z-index' : 100,
        'pointer-events' : 'auto',
        'opacity' : 1
      } );
    }
  };

  var logError = function( message ) {
    if ( window.console ) {
      window.console.error( message );
    }
  };

  $.fn.cbpQTRotator = function( options ) {
    if ( typeof options === 'string' ) {
      var args = Array.prototype.slice.call( arguments, 1 );
      this.each(function() {
        var instance = $.data( this, 'cbpQTRotator' );
        if ( !instance ) {
          logError( "cannot call methods on cbpQTRotator prior to initialization; " +
          "attempted to call method '" + options + "'" );
          return;
        }
        if ( !$.isFunction( instance[options] ) || options.charAt(0) === "_" ) {
          logError( "no such method '" + options + "' for cbpQTRotator instance" );
          return;
        }
        instance[ options ].apply( instance, args );
      });
    } 
    else {
      this.each(function() {  
        var instance = $.data( this, 'cbpQTRotator' );
        if ( instance ) {
          instance._init();
        }
        else {
          instance = $.data( this, 'cbpQTRotator', new $.CBPQTRotator( options, this ) );
        }
      });
    }
    return this;
  };

} )( jQuery, window );


/**
 * jquery.dropdown.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2012, Codrops
 * http://www.codrops.com
 */
;( function( $, window, undefined ) {

  'use strict';

  $.DropDown = function( options, element ) {
    this.$el = $( element );
    this._init( options );
  };

  // the options
  $.DropDown.defaults = {
    speed : 300,
    easing : 'ease',
    gutter : 0,
    // initial stack effect
    stack : true,
    // delay between each option animation
    delay : 0,
    // random angle and positions for the options
    random : false,
    // rotated [right||left||false] : the options will be rotated to thr right side or left side.
    // make sure to tune the transform origin in the stylesheet
    rotated : false,
    // effect to slide in the options. value is the margin to start with
    slidingIn : false,
    onOptionSelect : function(opt) { return false; }
  };

  $.DropDown.prototype = {

    _init : function( options ) {

      // options
      this.options = $.extend( true, {}, $.DropDown.defaults, options );
      this._layout();
      this._initEvents();

    },
    _layout : function() {

      var self = this;
      this.minZIndex = 1000;
      var value = this._transformSelect();
      this.opts = this.listopts.children( 'li' );
      this.optsCount = this.opts.length;
      this.size = { width : this.dd.width(), height : this.dd.height() };
      
      var elName = this.$el.attr( 'name' ), elId = this.$el.attr( 'id' ),
        inputName = elName !== undefined ? elName : elId !== undefined ? elId : 'cd-dropdown-' + ( new Date() ).getTime();

      this.inputEl = $( '<input type="hidden" name="' + inputName + '" value="' + value + '"></input>' ).insertAfter( this.selectlabel );
      
      this.selectlabel.css( 'z-index', this.minZIndex + this.optsCount );
      this._positionOpts();
      if( Modernizr.csstransitions ) {
        setTimeout( function() { self.opts.css( 'transition', 'all ' + self.options.speed + 'ms ' + self.options.easing ); }, 25 );
      }

    },
    _transformSelect : function() {

      var optshtml = '', selectlabel = '', value = -1;
      this.$el.children( 'option' ).each( function() {

        var $this = $( this ),
          val = isNaN( $this.attr( 'value' ) ) ? $this.attr( 'value' ) : Number( $this.attr( 'value' ) ) ,
          classes = $this.attr( 'class' ),
          selected = $this.attr( 'selected' ),
          link = $this.attr( 'href' ),
          label = $this.text();


        if( val !== -1 ) {
          optshtml += 
            classes !== undefined ? 
              '<li data-value="' + val + '"><a data-toggle="tab" href="' + link + '"><span class="' + classes + '">' + label + '</span></a></li>' :
              '<li data-value="' + val + '"><a data-toggle="tab" href="' + link + '"><span>' + label + '</span></a></li>';
        }

        if( selected ) {
          selectlabel = label;
          value = val;
        }

      } );

      this.listopts = $( '<ul/>' ).append( optshtml );
      this.selectlabel = $( '<span/>' ).append( selectlabel );
      this.dd = $( '<div class="cd-dropdown visible-phone"/>' ).append( this.selectlabel, this.listopts ).insertAfter( this.$el );
      this.$el.remove();

      return value;

    },
    _positionOpts : function( anim ) {

      var self = this;

      this.listopts.css( 'height', 'auto' );
      this.opts
        .each( function( i ) {
          $( this ).css( {
            zIndex : self.minZIndex + self.optsCount - 1 - i,
            top : self.options.slidingIn ? ( i + 1 ) * ( self.size.height + self.options.gutter ) : 0,
            left : 0,
            marginLeft : self.options.slidingIn ? i % 2 === 0 ? self.options.slidingIn : - self.options.slidingIn : 0,
            opacity : self.options.slidingIn ? 0 : 1,
            transform : 'none'
          } );
        } );

      if( !this.options.slidingIn ) {
        this.opts
          .eq( this.optsCount - 1 )
          .css( { top : this.options.stack ? 9 : 0, left : this.options.stack ? 4 : 0, width : this.options.stack ? this.size.width - 8 : this.size.width, transform : 'none' } )
          .end()
          .eq( this.optsCount - 2 )
          .css( { top : this.options.stack ? 6 : 0, left : this.options.stack ? 2 : 0, width : this.options.stack ? this.size.width - 4 : this.size.width, transform : 'none' } )
          .end()
          .eq( this.optsCount - 3 )
          .css( { top : this.options.stack ? 3 : 0, left : 0, transform : 'none' } );
      }

    },
    _initEvents : function() {
      
      var self = this;
      
      this.selectlabel.on( 'mousedown.dropdown', function( event ) {
        self.opened ? self.close() : self.open();
        return false;

      } );

      this.opts.on( 'click.dropdown', function() {
        if( self.opened ) {
          var opt = $( this );
          self.options.onOptionSelect( opt );
          self.inputEl.val( opt.data( 'value' ) );
          self.selectlabel.html( opt.html() );
          self.close();
        }
      } );

    },
    open : function() {
      var self = this;
      this.dd.toggleClass( 'cd-active' );
      this.listopts.css( 'height', ( this.optsCount + 1 ) * ( this.size.height + this.options.gutter ) );
      this.opts.each( function( i ) {

        $( this ).css( {
          opacity : 1,
          top : self.options.rotated ? self.size.height + self.options.gutter : ( i + 1 ) * ( self.size.height + self.options.gutter ),
          left : self.options.random ? Math.floor( Math.random() * 11 - 5 ) : 0,
          width : self.size.width,
          marginLeft : 0,
          transform : self.options.random ?
            'rotate(' + Math.floor( Math.random() * 11 - 5 ) + 'deg)' :
            self.options.rotated ?
              self.options.rotated === 'right' ?
                'rotate(-' + ( i * 5 ) + 'deg)' :
                'rotate(' + ( i * 5 ) + 'deg)'
              : 'none',
          transitionDelay : self.options.delay && Modernizr.csstransitions ? self.options.slidingIn ? ( i * self.options.delay ) + 'ms' : ( ( self.optsCount - 1 - i ) * self.options.delay ) + 'ms' : 0
        } );

      } );
      this.opened = true;

    },
    close : function() {

      var self = this;
      this.dd.toggleClass( 'cd-active' );
      if( this.options.delay && Modernizr.csstransitions ) {
        this.opts.each( function( i ) {
          $( this ).css( { 'transition-delay' : self.options.slidingIn ? ( ( self.optsCount - 1 - i ) * self.options.delay ) + 'ms' : ( i * self.options.delay ) + 'ms' } );
        } );
      }
      this._positionOpts( true );
      this.opened = false;

    }

  }

  $.fn.dropdown = function( options ) {
    var instance = $.data( this, 'dropdown' );
    if ( typeof options === 'string' ) {
      var args = Array.prototype.slice.call( arguments, 1 );
      this.each(function() {
        instance[ options ].apply( instance, args );
      });
    }
    else {
      this.each(function() {
        instance ? instance._init() : instance = $.data( this, 'dropdown', new $.DropDown( options, this ) );
      });
    }
    return instance;
  };

} )( jQuery, window );
